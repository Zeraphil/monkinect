
#include <algorithm>
#include <deque>
#include <vector>


using namespace std;

class MedianFilter{

public:
	MedianFilter();
	MedianFilter(int);
	MedianFilter(int, int);
	
	int medianAt(int index);

	void measure(vector<int> measurements);
	void update();
	void fill(int index, int value);
	

	const vector<int>& medians() const;

private:

	int m_window;
	vector< deque<int> > m_deques;
	vector< int > m_medians;

};