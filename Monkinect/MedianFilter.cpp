#include "MedianFilter.h"

int dequeMedian(deque<int> scores)
{
	  int median;
	  size_t size = scores.size();
	  sort(scores.begin(), scores.end());


	  if (size  % 2 == 0)
	  {
		  median = (scores[size / 2 - 1] + scores[size / 2]) / 2;
	  }
	  else 
	  {
		  median = scores[size / 2];
	  }

	  return median;
}

MedianFilter::MedianFilter()
{
	m_window = 10;
}

MedianFilter::MedianFilter(int values)
{
	m_window = 10;
	m_deques.resize(values, deque<int>(m_window, 1));
	m_medians.resize(values);
}

MedianFilter::MedianFilter(int values, int window)
{
	m_window = window;
	m_deques.resize(values, deque<int>(window, 1));
	m_medians.resize(values);
}

void MedianFilter::fill(int index, int value)
{
	if(index > m_deques.size()) return;

	m_deques[index].resize(m_window, value);
}

void MedianFilter::update()
{
	for(int m = 0; m < m_medians.size(); m++)
	{
		m_medians[m] = dequeMedian(m_deques[m]);
	}
}

void MedianFilter::measure(vector<int> measurements)
{
	for(int d = 0; d < m_deques.size(); d++)
	{
		m_deques[d].pop_front(); 
		m_deques[d].push_back(measurements[d]);
	}

	update();
}

int MedianFilter::medianAt(int index)
{
	return m_medians[index];
}

const vector<int>& MedianFilter::medians() const{
	return m_medians;
}