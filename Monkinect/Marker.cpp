#include "Marker.h"

Point2d setRectCenter(Rect rect)
{
	return (Point(rect.tl().x + rect.width/2, rect.tl().y + rect.height/2 ));
}

template <class T>
inline double getDistance(T & t1, T & t2)
{
	//distance formula for comparison, no sqrt necessary
	//OpenCV has a pow, disambiguating

	return (std::pow((t2.x - t1.x),2) + 
		    std::pow((t2.y - t1.y),2) +
			std::pow((t2.z - t1.z),2)); 
}

Marker::Marker ()
{
	m_hueV = 0;
}

Marker::Marker(Rect rect, double depth, Scalar hsv, int frames, int ID)
{
	m_rect = rect;
	m_hueV =  hsv[0];
	m_id = ID;

	m_occluded = false;
	m_framesOccluded = 0;
	m_framesSliding = frames;
	m_radius = 10000.0;

	Point center = setRectCenter(rect);
	m_center.x = center.x;
	m_center.y = center.y;
	m_center.z = depth;

	m_hsv = hsv;

	initMedian();
	initKalman();
}


Marker::Marker(Rect rect, double depth, Scalar hsv, Mat hist, int frames, int ID)
{
	m_rect = rect;
	m_hueV =  hsv[0];
	m_id = ID;

	m_occluded = false;
	m_framesOccluded = 0;
	m_framesSliding = frames;
	m_radius = 10000.0;

	Point center = setRectCenter(rect);
	m_center.x = center.x;
	m_center.y = center.y;
	m_center.z = depth;

	m_hsv = hsv;
	m_hist = hist;

	initMedian();
	initKalman();
}

void Marker::initMedian()
{
	MedianFilter MF(3, m_framesSliding);
	MF.fill(0, m_center.x);
	MF.fill(1, m_center.y);
	MF.fill(2, m_center.z);

	MF.update();

	m_medianfilter = MF;

}

//MARKER KALMAN FILTER
void Marker::initKalman()
{
// Initialize Kalman Filter
	KalmanFilter KF(6, 3, 0);
	double dt = 1.0 / 30.0;

	KF.statePost.at<float>(0) = m_center.x;
	KF.statePost.at<float>(1) = m_center.y;
	KF.statePost.at<float>(2) = m_center.z;
	KF.statePost.at<float>(3) = 0;
	KF.statePost.at<float>(4) = 0;
	KF.statePost.at<float>(5) = 0;
	KF.transitionMatrix = *(Mat_<float>(6, 6) << 1, 0, 0, dt, 0, 0,
												 0, 1, 0, 0, dt, 0,
												 0, 0, 1, 0, 0, dt,
												 0, 0, 0, 1, 0,  0,
												 0, 0, 0, 0, 1,  0,
												 0, 0, 0, 0, 0,  1);

    setIdentity(KF.measurementMatrix);
    setIdentity(KF.processNoiseCov, Scalar::all(1e-3));//Q
    setIdentity(KF.measurementNoiseCov, Scalar::all(1e-3));//R
    setIdentity(KF.errorCovPost, Scalar::all(.1));

	m_kalmanfilter = KF;
}


void Marker::predict()
{
	m_kalmanfilter.predict();
	m_predicted =  Point3d(m_kalmanfilter.statePre.at<float>(0), m_kalmanfilter.statePre.at<float>(1),  m_kalmanfilter.statePre.at<float>(2));
	m_median    =  Point3d(m_medianfilter.medianAt(0), m_medianfilter.medianAt(1), m_medianfilter.medianAt(2));
}

//MARKER UPDATE FUNCTIONS


void Marker::update(Rect rect, double depth)
{
	//Update marker position in 3D space without blob data

	if(rect.area() > 0)
	{
		//obvious assignments
		m_size = rect.area();
		m_rect = rect;
		m_depth = depth;
		m_occluded = false;

		//calculate true value
		Point center = setRectCenter(rect);
		m_center = Point3d(center.x, center.y, depth);

		//update filters
		Mat measurement = (Mat_<float> (3,1) << m_center.x, m_center.y,  m_center.z);
		m_kalmanfilter.correct(measurement);

		vector<int> values (3,0);
		values[0] = center.x;
		values[1] = center.y;
		values[2] = static_cast<int>(depth);

		m_medianfilter.measure(values);

	}
	else
	{

		m_occluded = true;
	}
}

void Marker::update(RotatedRect rotated, Rect rect, double depth)
{
	//Update marker position in 3D space without blob data, get rotation

	if(rect.area() > 0)
	{
		m_size = rect.area();
		m_rect = rect;
		m_rotated = rotated;
		Point center = rotated.center;

		m_center = Point3d(center.x, center.y, depth);

		m_depth = depth;

		m_occluded = false;

		Mat measurement = (Mat_<float> (3,1) << m_center.x, m_center.y,  m_center.z);
		m_kalmanfilter.correct(measurement);



	}
	else
	{
		m_occluded = true;
	}
}

void Marker::update(Rect rect, double depth, Scalar scalar)
{
	//Update marker position in 3D space without blob data, update color dynamically

	if(rect.area() > 0)
	{
		m_occluded = false;

		m_size = rect.area();
		m_rect = rect;

		//Positional info
		Point center = setRectCenter(rect);
		m_center = Point3d(center.x, center.y, depth);
		m_depth = depth;

		Mat measurement = (Mat_<float> (3,1) << m_center.x, m_center.y,  m_center.z);
		m_kalmanfilter.correct(measurement);

		//Color info
		//hueDeque.pop_front(); hueDeque.push_back(scalar[0]);
		//satDeque.pop_front(); satDeque.push_back(scalar[1]);
		//valDeque.pop_front(); valDeque.push_back(scalar[2]);

		//m_hsv = Scalar(m_hueV, dequeMedian(satDeque), dequeMedian(valDeque));

	}
	else
	{
		m_occluded = true;
	}
}

int Marker::estimateCost(Point3d position, int area)
{
	//evaluation function
	//estimate Euclidean distance of linear combination of weighed Kalman and median filter points to target and the size of the target. 
	double weights[2] = {0.65, 0.35};
	//the smaller the target the higher the cost
	double cost = std::floor((getDistance(m_predicted, position) * weights[0] + (getDistance(m_median, position) * weights[1]))/(2*area));

	return static_cast<int>(cost);
}

void Marker::searchRadius(double radius)
{
	m_radius = radius;
}

void Marker::raiseOcclusion()
{
	m_occluded = true;
	m_framesOccluded = m_framesOccluded + 1;
}

void Marker::resetOcclusion()
{
	m_framesOccluded = 0;
	m_occluded = false;
}

//END MARKER KALMAN FILTER

//MARKER ACCESSORS

Rect Marker::rect() const
{
	return m_rect;
}

void Marker::rect(Rect rect)
{
	m_rect = rect;
}

bool Marker::isOccluded() const
{
	return m_occluded;
}

int Marker::hueValue() const
{
	return m_hueV;
}

int Marker::ID() const
{
	return m_id;
}

int Marker::framesOccluded() const
{
	return m_framesOccluded;
}

Point3d Marker::center() const
{

	return m_center;
}

double Marker::depth() const
{
	return m_depth;
}

double Marker::size() const
{
	return m_size;
}

double Marker::searchRadius() const
{
	return m_radius;
}

Point3d Marker::predictedCenter() const
{
	return m_predicted;
}

Point3d Marker::filteredCenter() const
{
	return m_median;
}

const Scalar& Marker::markerHSV() const
{
	return m_hsv;
}

const Mat& Marker::markerHist() const
{
	return m_hist;
}

const RotatedRect& Marker::rotatedRect() const
{
	return m_rotated;
}


//END MARKER ACCESSORS