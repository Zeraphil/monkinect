
#include "MarkerBuffer.pb.h"
#include <google/protobuf/io/zero_copy_stream_impl.h>

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;


template <typename TMessage> 
bool parse_delimited(std::istream& stream, TMessage& message) 
{ 
    uint32_t messageSize = 0; 
    // Read the message size. 
    { 
        google::protobuf::io::IstreamInputStream istreamWrapper(&stream, sizeof(uint32_t)); 
        google::protobuf::io::CodedInputStream codedIStream(&istreamWrapper); 
        // Don't consume more than sizeof(uint32_t) from the stream. 
        google::protobuf::io::CodedInputStream::Limit oldLimit = codedIStream.PushLimit(sizeof(uint32_t)); 
        codedIStream.ReadLittleEndian32(&messageSize); 
        codedIStream.PopLimit(oldLimit); 
        assert(messageSize > 0); 
        assert(istreamWrapper.ByteCount() == sizeof(uint32_t)); 
    } 
    // Read the message. 
    { 
        google::protobuf::io::IstreamInputStream istreamWrapper(&stream, messageSize); 
        google::protobuf::io::CodedInputStream codedIStream(&istreamWrapper); 
        // Read the message, but don't consume more than messageSize bytes from the stream. 
        google::protobuf::io::CodedInputStream::Limit oldLimit = codedIStream.PushLimit(messageSize); 
        message.ParseFromCodedStream(&codedIStream); 
        codedIStream.PopLimit(oldLimit); 
        assert(istreamWrapper.ByteCount() == messageSize); 
    } 
	return stream.good(); 
} 

void printCommandLineParams()
{
	cout<< "Output: timestamp frame firstmarkerID occluded? x y z etc..."<<endl;
    cout << "-r        Filename of the track file. The data will grabbed from it." << endl ;
}

std::string binToTxt (std::string infile)
{
	int lastindex = infile.find_last_of("."); 
	string rawname = infile.substr(0, lastindex); 
	rawname = rawname + "_matrix.txt";

	return rawname;
}


int main(int argc, char* argv[]) {
  // Verify that the version of the library that we linked against is
  // compatible with the version of the headers we compiled against.
  GOOGLE_PROTOBUF_VERIFY_VERSION;

	std::string filename;

    for( int i = 1; i < argc; i++ )
	{
		if( !strcmp( argv[i], "--help" ) || !strcmp( argv[i], "-h" ) )
		{
			printCommandLineParams();
			exit(0);
		}
		else if( !strcmp( argv[i], "-r" ) )
				{
					filename = argv[++i];
				}
	}

   ifstream input(filename, ios::in | ios::binary);
   std::string str;

	input.seekg(0, std::ios::end);   
	str.reserve(input.tellg());
	input.seekg(0, std::ios::beg);

	str.assign((std::istreambuf_iterator<char>(input)),
            std::istreambuf_iterator<char>());

	istringstream strinput(str);

    MarkerBuffer::Tracker buffer;

   if (input.is_open())
   {
	 /*write to text file and read with dlmread in matlab*/

	ofstream output(binToTxt(filename));

    while ( parse_delimited(strinput, buffer))
	{
		output<<buffer.mutable_header()->timestamp()<<" "<<buffer.mutable_header()->frame()<<" ";

		for( int i = 0; i < buffer.marker().size(); i++)
		{
			output<<buffer.marker(i).id()<<" "<<buffer.marker(i).occluded() << " "<<buffer.marker(i).x()<<" "<<buffer.marker(i).y()<<" "<<buffer.marker(i).z()<<" ";
		}
		output<<endl;
	}
	output.close();
	input.close();

   }


  // Optional:  Delete all global objects allocated by libprotobuf.
  google::protobuf::ShutdownProtobufLibrary();
cout << "Press anykey to exit.";
cin.ignore();
cin.get();
  return 0;
}

